/*
 * Autor       : Mikael Nilsson
 * Filename    : IncludeCatch.cpp
 * Description : Används för att minska på kompieringstiden genom
 * att definera CATCH_CONFIG_MAIN i en kompileringsenhet
 * som aldrig ändras och därför aldrig behöver kompileras om.
 *
*/

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "MathExpression.h"


TEST_CASE("Test calculator result","[Classic]"){

    MathExpression me;
    /**
     * this Test case is for test calculating final result and in the case of need you can add more test cases
     **/
    REQUIRE(me.postfixCalculator(me.convertToPostfix(me.tokenizer("7+(9*6-4/2)-20/4"))) == 54);
    REQUIRE(me.postfixCalculator(me.convertToPostfix(me.tokenizer("1565127+(9*6-4/2)- 20/4"))) == 54);
    REQUIRE(me.postfixCalculator(me.convertToPostfix(me.tokenizer("7+(9*6-4///2)-20/4"))) == 54);
    REQUIRE(me.postfixCalculator(me.convertToPostfix(me.tokenizer("7+(9   *6-4/2)-20/4"))) == 54);

}

