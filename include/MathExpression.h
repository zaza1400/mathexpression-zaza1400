/*
 * Autor       : Mikael Nilsson
 * Filename    : MathExpression.h
 * Description : Klass för lagring och beräkning av enkla matematiska uttryck.
 *
*/

#ifndef MATHEXPRESSION_H
#define MATHEXPRESSION_H

#include <string>
#include <vector>
#include <iostream>

class MathExpression {
public:

    //MathExpression(const std::string &expression);

    MathExpression& operator=(const std::string& expression);

    void vectorshower(std::vector<std::string> vc) {
        for (int i = 0; i < vc.size(); i++) {
            if (vc.at(i) != "(" || vc.at(i) != ")")
                std::cout << vc.at(i) << " ";
        }
        std::cout << "\n";
    }

    int getPrior(char c) {
        if (c == '*' || c == '/')
            return 2;
        if (c == '+' || c == '-')
            return 1;
    }

    bool isOperator(std::string c) {
        return c == "+" || c == "-" || c == "*" || c == "/";
    }

    bool isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/';
    }

    bool isOperand(std::string c) {
        if (!(isOperator(c)))
            return (c != ")") && (c != "(");
        return false;
    }


    int calculate(char c, int a, int b) {
        switch (c) {
            case '+':
                return (a + b);
            case '-':
                return (a - b);
            case '*':
                return (a * b);
            case '/':
                return (a / b);
        }
    }

    int postfixCalculator(std::vector<std::string> postfixes) {
        std::vector<int> operands;
        int a = 0, b = 0, c = 0;
        for (int i = 0; i < postfixes.size(); i++) {
            if (isOperand(postfixes[i]))
                operands.push_back(std::stoi(postfixes[i]));
            else if (operands.size() > 1) {
                b = operands.back();
                operands.pop_back();
                a = operands.back();
                operands.pop_back();
                c = calculate(postfixes[i][0], a, b);
                operands.push_back(c);
            }
        }
        return operands[0];
    }

    std::vector<std::string> convertToPostfix(std::vector<std::string> infix) {
        std::vector<std::string> operators;
        std::vector<std::string> postfixes;

        for (int i = 0; i < infix.size(); i++) {
            if (isOperand(infix[i]))
                postfixes.push_back(infix[i]);

            else if (infix[i] == "(") {
                operators.push_back(infix[i]);
            } else if (infix[i] == ")") {
                while (!operators.empty() && operators.back()[0] != '(') {
                    postfixes.push_back(std::string(1, operators.back()[0]));
                    operators.pop_back();
                }
                operators.pop_back();
            } else if (isOperator(infix[i])) {
                while (!operators.empty() && operators.back()[0] != '(' &&
                       getPrior(operators.back()[0]) >= getPrior(infix[i][0])) {
                    postfixes.push_back(std::string(1, operators.back()[0]));
                    operators.pop_back();
                }
                operators.push_back(infix[i]);
            }
        }

        while (operators.size() != 0) {
            postfixes.push_back(std::string(1, operators.back()[0]));
            operators.pop_back();
        }
        return postfixes;
    }

    std::vector<std::string> tokenizer(std::string str) {
        std::string tempstr = "";
        std::vector<std::string> vc;
        for (int i = 0; i < str.length(); i++) {
            if (str[i] == ' ')
                continue;
            if (str[i] == '(' || str[i] == ')') {
                tempstr = str[i];
                vc.push_back(tempstr);
                tempstr = "";
                continue;
            }

            if (isOperator(str[i])) {
                tempstr = "";
                tempstr += str[i];
                vc.push_back(tempstr);
                tempstr = "";
            } else {
                while (i < str.length()) {
                    tempstr += str[i];
                    i++;
                    if (isOperator(str[i]) || str[i] == '(' || str[i] == ')') {
                        i--;
                        break;
                    }
                }
                vc.push_back(tempstr);
                tempstr = "";
            }
        }
        return vc;
    }

    bool isValid() const {

        std::cout << "Valid Expression" << std::endl;
    }

    std::string errorMessage() const {
        std::cout << "invalid Expression" << std::endl;
    }


private:


};

#endif // MATHEXPRESSION_H
