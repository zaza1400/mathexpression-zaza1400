/*
 * Autor       : Mikael Nilsson
 * Filename    : main.cpp
 * Description : Exempel program för att interaktivt testa MathExpresion
 *
*/

//#include <memstat.hpp>
#include <iostream>
#include <sstream>
#include <stack>
#include <cctype>

using namespace std;

#include "MathExpression.h"

int main() {


    cout << "MathExpression REPL (Read-Eval-Print-Loop)." << endl << "Type quit to exit." << endl;

    string input;

    cout << "Enter a math expression : ";
    getline(cin, input);
    //while (input != "quit") {

    while (input != "quit") {
        MathExpression me;

        if (me.isValid()) {
            std::cout << "__one__" << endl;
            me.vectorshower(me.tokenizer(input));
            std::cout << "__two__" << endl;
            me.vectorshower(me.convertToPostfix(me.tokenizer(input)));
            std::cout << "__three__" << endl;
            std::vector<string> inf;
            std::cout << me.postfixCalculator(me.convertToPostfix(me.tokenizer(input)));

        } else {
            me.errorMessage();
        }

        cout << "Enter a math expression : ";
        getline(cin, input);
    }

    return 0;
}

